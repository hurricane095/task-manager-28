package ru.krivotulov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.repository.IUserOwnedRepository;
import ru.krivotulov.tm.api.service.IUserOwnedService;
import ru.krivotulov.tm.enumerated.Sort;
import ru.krivotulov.tm.exception.field.IdEmptyException;
import ru.krivotulov.tm.exception.field.IndexIncorrectException;
import ru.krivotulov.tm.exception.field.UserIdEmptyException;
import ru.krivotulov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

/**
 * AbstractUserOwnedService
 *
 * @author Aleksey_Krivotulov
 */
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final R repository) {
        super(repository);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId,
                 @Nullable final M model
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        return repository.add(userId, model);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId,
                           @Nullable final Comparator<M> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) repository.findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId,
                         @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId,
                            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public M delete(@Nullable final String userId,
                    @Nullable final M model
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        return repository.delete(userId, model);
    }

    @Nullable
    @Override
    public M deleteById(@Nullable final String userId,
                        @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.deleteById(userId, id);
    }

    @Nullable
    @Override
    public M deleteByIndex(@Nullable final String userId,
                           @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.deleteByIndex(userId, index);
    }

    @Override
    public boolean existsById(@Nullable final String userId,
                              @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(userId, id);
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId,
                           @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return repository.findAll(userId, sort.getComparator());
    }

}
