package ru.krivotulov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

/**
 * Role
 *
 * @author Aleksey_Krivotulov
 */
public enum Role {
    USUAL("Usual user"),
    ADMINISTRATOR("Administrator");

    @NotNull
    private final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
