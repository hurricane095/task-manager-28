package ru.krivotulov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * ProjectUpdateByIdCommand
 *
 * @author Aleksey_Krivotulov
 */
public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-update-by-id";

    @NotNull
    public static final String DESCRIPTION = "Update project by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        @Nullable final String id = TerminalUtil.readLine();
        System.out.println("ENTER NAME: ");
        @Nullable final String name = TerminalUtil.readLine();
        System.out.println("ENTER DESCRIPTION: ");
        @Nullable final String description = TerminalUtil.readLine();
        @NotNull final String userId = getUserId();
        getProjectService().updateById(userId, id, name, description);
    }

}
