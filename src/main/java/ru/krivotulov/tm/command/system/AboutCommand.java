package ru.krivotulov.tm.command.system;

import org.jetbrains.annotations.NotNull;

/**
 * AboutCommand
 *
 * @author Aleksey_Krivotulov
 */
public class AboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String DESCRIPTION = "Display developer info.";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println(getPropertyService().getAuthorName());
        System.out.println(getPropertyService().getAuthorEmail());
    }

}
