package ru.krivotulov.tm.util;

import org.jetbrains.annotations.NotNull;

import java.lang.management.ManagementFactory;

/**
 * SystemUtil
 *
 * @author Aleksey_Krivotulov
 */
public interface SystemUtil {

    static long getPID() {
        @NotNull final String processName =
                ManagementFactory.getRuntimeMXBean().getName();
        if (processName != null && processName.length() > 0) {
            try {
                return Long.parseLong(processName.split("@")[0]);
            } catch (@NotNull final Exception e) {
                return 0;
            }
        }
        return 0;
    }

}
